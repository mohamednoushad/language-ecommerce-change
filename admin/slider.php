<?php require_once('header.php'); ?>

<section class="content-header">
	<div class="content-header-left">
		<h1><?php echo LANG_VALUE_308; ?></h1>
	</div>
	<div class="content-header-right">
		<!-- <a href="slider-add.php" class="btn btn-primary btn-sm">Add Slider</a> -->
	</div>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box-body table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><?php echo LANG_VALUE_309; ?></th>
								<th><?php echo LANG_VALUE_310; ?></th>
								<th><?php echo LANG_VALUE_311; ?></th>
								<th><?php echo LANG_VALUE_312; ?></th>
								<th><?php echo LANG_VALUE_313; ?></th>
								<th><?php echo LANG_VALUE_314; ?></th>
								<th><?php echo LANG_VALUE_315; ?></th>
								<th><?php echo LANG_VALUE_316; ?></th>
								<th><?php echo LANG_VALUE_317; ?></th>
								<th><?php echo LANG_VALUE_318; ?></th>
								<th width="140"><?php echo LANG_VALUE_319; ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i=0;
							$statement = $pdo->prepare("SELECT
														
														id,
														photo,
														heading,
														heading_chinese,
														content,
														content_chinese,
														button_text,
														button_text_chinese,
														button_url,
														position

							                           	FROM tbl_slider
							                           	
							                           	");
							$statement->execute();
							$result = $statement->fetchAll(PDO::FETCH_ASSOC);							
							foreach ($result as $row) {
								$i++;
								?>
								<tr>
									<td><?php echo $i; ?></td>
									<td style="width:150px;"><img src="../assets/uploads/<?php echo $row['photo']; ?>" alt="<?php echo $row['heading']; ?>" style="width:140px;"></td>
									<td><?php echo $row['heading']; ?></td>
									<td><?php echo $row['heading_chinese']; ?></td>
									<td><?php echo $row['content']; ?></td>
									<td><?php echo $row['content_chinese']; ?></td>
									<td><?php echo $row['button_text']; ?></td>
									<td><?php echo $row['button_text_chinese']; ?></td>
									<td><?php echo $row['button_url']; ?></td>
									<td><?php echo $row['position']; ?></td>
									<td>										
										<a href="slider-edit.php?id=<?php echo $row['id']; ?>" class="btn btn-primary btn-xs"><?php echo LANG_VALUE_320; ?></a>
										<!-- <a href="#" class="btn btn-danger btn-xs" data-href="slider-delete.php?id=<?php echo $row['id']; ?>" data-toggle="modal" data-target="#confirm-delete">Delete</a>   -->
									</td>
								</tr>
								<?php
							}
							?>							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>


<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete this item?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>

<?php require_once('footer.php'); ?>