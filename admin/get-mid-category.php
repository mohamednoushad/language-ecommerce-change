<?php
ob_start();
session_start();
include 'inc/config.php';

$selected_language = '';

if(isset($_SESSION['customer'])) {

	$selected_language = $_SESSION['user'] ['language'];
	
} else if (isset($_SESSION ['language'])) {

	$selected_language = $_SESSION ['language'];

} else {

	$selected_language = $default_language;

}

// Getting all language variables into array as global variable
$i=1;
$statement = $pdo->prepare("SELECT * FROM tbl_language");
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_ASSOC);	
if($selected_language == 'English') {
	foreach ($result as $row) {
		define('LANG_VALUE_'.$i,$row['lang_value']);
		$i++;
	}
} else {
	foreach ($result as $row) {
		define('LANG_VALUE_'.$i,$row['lang_value_chinese']);
		$i++;
	}
}



if($_POST['id'])
{
	$id = $_POST['id'];
	
	$statement = $pdo->prepare("SELECT * FROM tbl_mid_category WHERE tcat_id=?");
	$statement->execute(array($id));
	$result = $statement->fetchAll(PDO::FETCH_ASSOC);
	?><option value=""><?php echo LANG_VALUE_382; ?></option><?php						
	foreach ($result as $row) {
		?>
		<option value="<?php echo $row['mcat_id']; ?>">
		<?php
		if($selected_language == 'English') {
			echo $row['mcat_name']; 
		} else {
			echo $row['mcat_name_chinese'];	
		}
		 ?>
		</option>
        <?php
	}
}