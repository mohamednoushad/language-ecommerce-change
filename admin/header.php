<?php
ob_start();
session_start();
include("inc/config.php");
include("inc/functions.php");
include("inc/CSRF_Protect.php");
$csrf = new CSRF_Protect();
$error_message = '';
$success_message = '';
$error_message1 = '';
$success_message1 = '';

$selected_language = '';

if(isset($_SESSION['customer'])) {

	$selected_language = $_SESSION['user'] ['language'];
	
} else if (isset($_SESSION ['language'])) {

	$selected_language = $_SESSION ['language'];

} else {

	$selected_language = $default_language;

}


// Getting all language variables into array as global variable
$i=1;
$statement = $pdo->prepare("SELECT * FROM tbl_language");
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_ASSOC);	
if($selected_language == 'English') {
	foreach ($result as $row) {
		define('LANG_VALUE_'.$i,$row['lang_value']);
		$i++;
	}
} else {
	foreach ($result as $row) {
		define('LANG_VALUE_'.$i,$row['lang_value_chinese']);
		$i++;
	}
}

// Check if the user is logged in or not
if(!isset($_SESSION['user'])) {
	header('location: login.php');
	exit;
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo LANG_VALUE_176; ?></title>

	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/ionicons.min.css">
	<link rel="stylesheet" href="css/datepicker3.css">
	<link rel="stylesheet" href="css/all.css">
	<link rel="stylesheet" href="css/select2.min.css">
	<link rel="stylesheet" href="css/dataTables.bootstrap.css">
	<link rel="stylesheet" href="css/jquery.fancybox.css">
	<link rel="stylesheet" href="css/AdminLTE.min.css">
	<link rel="stylesheet" href="css/_all-skins.min.css">
	<link rel="stylesheet" href="css/on-off-switch.css"/>
	<link rel="stylesheet" href="css/summernote.css">
	<link rel="stylesheet" href="style.css">

</head>

<body class="hold-transition fixed skin-blue sidebar-mini">

	<div class="wrapper">

		<header class="main-header">

			<a href="index.php" class="logo">
				<span class="logo-lg"><?php echo LANG_VALUE_176; ?></span>
			</a>

			<nav class="navbar navbar-static-top">
				
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>

				<span style="float:left;line-height:50px;color:#fff;padding-left:15px;font-size:18px;"><?php echo LANG_VALUE_176; ?></span>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="../assets/uploads/<?php echo $_SESSION['user']['photo']; ?>" class="user-image" alt="User Image">
								<span class="hidden-xs"><?php echo $_SESSION['user']['full_name']; ?></span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-footer">
									<div>
										<a href="profile-edit.php" class="btn btn-default btn-flat"><?php echo LANG_VALUE_179; ?></a>
									</div>
									<div>
										<a href="logout.php" class="btn btn-default btn-flat"><?php echo LANG_VALUE_14; ?></a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>

			</nav>
		</header>

  		<?php $cur_page = substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); ?>

  		<aside class="main-sidebar">
    		<section class="sidebar">
      
      			<ul class="sidebar-menu">

			        <li class="treeview <?php if($cur_page == 'index.php') {echo 'active';} ?>">
			          <a href="index.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_89; ?></span>
			          </a>
			        </li>

					
			        <li class="treeview <?php if( ($cur_page == 'settings.php') ) {echo 'active';} ?>">
			          <a href="settings.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_180; ?></span>
			          </a>
			        </li>

			        <li class="treeview <?php if( ($cur_page == 'slider.php') ) {echo 'active';} ?>">
			          <a href="slider.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_181; ?></span>
			          </a>
			        </li>

			        <li class="treeview <?php if( ($cur_page == 'service.php') ) {echo 'active';} ?>">
			          <a href="service.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_182; ?></span>
			          </a>
			        </li>

			        <li class="treeview <?php if( ($cur_page == 'testimonial.php') ) {echo 'active';} ?>">
			          <a href="testimonial.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_183; ?></span>
			          </a>
			        </li>

			        <!-- <li class="treeview <?php if( ($cur_page == 'faq.php') ) {echo 'active';} ?>">
			          <a href="faq.php">
			            <i class="fa fa-hand-o-right"></i> <span>FAQ</span>
			          </a>
			        </li> -->

			        <li class="treeview <?php if( ($cur_page == 'photo.php') || ($cur_page == 'video.php') ) {echo 'active';} ?>">
						<a href="#">
							<i class="fa fa-hand-o-right"></i>
							<span><?php echo LANG_VALUE_205; ?></span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="photo.php"><i class="fa fa-circle-o"></i> <?php echo LANG_VALUE_184; ?></a></li>
							<li><a href="video.php"><i class="fa fa-circle-o"></i> <?php echo LANG_VALUE_185; ?></a></li>
						</ul>
					</li>

					<!-- <li class="treeview <?php if( ($cur_page == 'post.php') ||($cur_page == 'post-add.php') ||($cur_page == 'post-edit.php') || ($cur_page == 'category.php') || ($cur_page == 'category-add.php') || ($cur_page == 'category-edit.php') ) {echo 'active';} ?>">
						<a href="#">
							<i class="fa fa-hand-o-right"></i>
							<span>Blog Posts</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="category.php"><i class="fa fa-circle-o"></i> Category</a></li>
							<li><a href="post.php"><i class="fa fa-circle-o"></i> Posts</a></li>
						</ul>
					</li> -->

					<li class="treeview <?php if( ($cur_page == 'size.php') || ($cur_page == 'size-add.php') || ($cur_page == 'size-edit.php') || ($cur_page == 'color.php') || ($cur_page == 'color-add.php') || ($cur_page == 'color-edit.php') || ($cur_page == 'country.php') || ($cur_page == 'country-add.php') || ($cur_page == 'country-edit.php') || ($cur_page == 'shipping-cost.php') || ($cur_page == 'shipping-cost-edit.php') || ($cur_page == 'top-category.php') || ($cur_page == 'top-category-add.php') || ($cur_page == 'top-category-edit.php') || ($cur_page == 'mid-category.php') || ($cur_page == 'mid-category-add.php') || ($cur_page == 'mid-category-edit.php') || ($cur_page == 'end-category.php') || ($cur_page == 'end-category-add.php') || ($cur_page == 'end-category-edit.php') ) {echo 'active';} ?>">
						<a href="#">
							<i class="fa fa-hand-o-right"></i>
							<span><?php echo LANG_VALUE_186; ?></span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="size.php"><i class="fa fa-circle-o"></i> <?php echo LANG_VALUE_187; ?></a></li>
							<li><a href="color.php"><i class="fa fa-circle-o"></i> <?php echo LANG_VALUE_188; ?></a></li>
							<li><a href="country.php"><i class="fa fa-circle-o"></i> <?php echo LANG_VALUE_189; ?></a></li>
							<li><a href="shipping-cost.php"><i class="fa fa-circle-o"></i> <?php echo LANG_VALUE_190; ?></a></li>
							<li><a href="top-category.php"><i class="fa fa-circle-o"></i> <?php echo LANG_VALUE_191; ?></a></li>
							<li><a href="mid-category.php"><i class="fa fa-circle-o"></i> <?php echo LANG_VALUE_192; ?></a></li>
							<li><a href="end-category.php"><i class="fa fa-circle-o"></i> <?php echo LANG_VALUE_193; ?></a></li>
						</ul>
					</li>


					<li class="treeview <?php if( ($cur_page == 'product.php') || ($cur_page == 'product-add.php') || ($cur_page == 'product-edit.php') ) {echo 'active';} ?>">
			          <a href="product.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_194; ?></span>
			          </a>
			        </li>


			        <li class="treeview <?php if( ($cur_page == 'order.php') ) {echo 'active';} ?>">
			          <a href="order.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_195; ?></span>
			          </a>
			        </li>


			        <!-- <li class="treeview <?php if( ($cur_page == 'rating.php') ) {echo 'active';} ?>">
			          <a href="rating.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_196; ?></span>
			          </a>
			        </li> -->

			        <li class="treeview <?php if( ($cur_page == 'language.php') ) {echo 'active';} ?>">
			          <a href="language.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_197; ?></span>
			          </a>
			        </li>
					
					<!-- <li class="treeview <?php if( ($cur_page == 'customer-message.php') ) {echo 'active';} ?>">
						<a href="#">
							<i class="fa fa-hand-o-right"></i>
							<span><?php echo LANG_VALUE_198; ?></span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li><a href="customer-message.php"><i class="fa fa-circle-o"></i> <?php echo LANG_VALUE_199; ?></a></li>
						</ul>
					</li> -->


					<li class="treeview <?php if( ($cur_page == 'customer.php') || ($cur_page == 'customer-add.php') || ($cur_page == 'customer-edit.php') ) {echo 'active';} ?>">
			          <a href="customer.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_200; ?></span>
			          </a>
			        </li>

			        <li class="treeview <?php if( ($cur_page == 'page.php') ) {echo 'active';} ?>">
			          <a href="page.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_201; ?></span>
			          </a>
			        </li>

			        <li class="treeview <?php if( ($cur_page == 'social-media.php') ) {echo 'active';} ?>">
			          <a href="social-media.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_202; ?></span>
			          </a>
			        </li>

			        <li class="treeview <?php if( ($cur_page == 'advertisement.php') ) {echo 'active';} ?>">
			          <a href="advertisement.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_203; ?></span>
			          </a>
			        </li>

			        <!-- <li class="treeview <?php if( ($cur_page == 'subscriber.php')||($cur_page == 'subscriber.php') ) {echo 'active';} ?>">
			          <a href="subscriber.php">
			            <i class="fa fa-hand-o-right"></i> <span><?php echo LANG_VALUE_204; ?></span>
			          </a>
			        </li> -->


			        

			        


      			</ul>
    		</section>
  		</aside>

  		<div class="content-wrapper">