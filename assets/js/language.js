// function changeLanguage() {
//     console.log("called change language function");
// }

// $('#main-li').on('click', function () {
//     console.log("clicked");
//     // console.log($(this).attr('id'));
//     // $(this).addClass('active-class').siblings().removeClass('active-class');
// });


function changeLanguageToEnglish() {
    console.log("change language to english called");
    $.ajax({
        type: "POST",
        url: "change-language.php",
        data: {
            toLanguage: "English"
        },
        success: function (data) {
            console.log("success");
            console.log(data);
            location.reload();
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function changeLanguageToChinese() {
    console.log("change language to chinese called");
    $.ajax({
        type: "POST",
        url: "change-language.php",
        data: {
            toLanguage: "Chinese"
        },
        success: function (data) {
            console.log("success");
            console.log(data);
            location.reload();
        },
        error: function (error) {
            console.log(error);
        }
    });
}